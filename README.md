# GooCocoonJSWebView

This is a project to test Goo in combination with CocoonJS and the WebView feature to display a HTML based UI on top of the main 3D scene.


## Some useful links:

[Development Guide](http://support.ludei.com/hc/en-us/articles/200767118-Development-Guide)

[CocoonJS & Webview](http://support.ludei.com/hc/en-us/articles/200807797-CocoonJS-Webview)

[CocoonJS Demo Apps](https://cocoonjsservice.ludei.com/cocoonjslaunchersvr/demo-list/)

[How to use](http://support.ludei.com/hc/en-us/articles/201048463-How-to-use)

[ScreenCanvas](http://support.ludei.com/hc/en-us/articles/201810268-ScreenCanvas)

Ludei CocoonJSExtensions on [Bitbucket](https://bitbucket.org/ludei/cocoon_cocoonjsextensions/src)


## Some issues I encountered:

If I rename the WV.html file to webview.html the screen is black if cloud compiled and no §D scene is visible.

If I add the standard Goo style: #goo {position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;width: 100%;height: 100%;text-transform: uppercase;}
The web view menu is not visible

